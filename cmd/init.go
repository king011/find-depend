package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/king011/find-depend/version"
	"log"
	"os"
	"runtime"
	"strings"
)

const (
	// App 程式名
	App = "find-depend"
)

var _v bool
var _platform string
var _nocopy bool
var _bin []string
var _overlay bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "find-depend find dependency tools",
	Run: func(cmd *cobra.Command, args []string) {
		if _v {
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			return
		} else if len(args) != 0 {
			if _platform != "windows" {
				log.Fatalf("not support platform '%s'\n", _platform)
			}
			yes := false
			for _, str := range args {
				str = strings.TrimSpace(str)
				if str == "" {
					continue
				}
				yes = true
				c := newContext(_platform, _nocopy, _overlay, _bin, str)
				c.Done()
			}
			if yes {
				return
			}
		}
		fmt.Println(runtime.GOOS, runtime.GOARCH)
		fmt.Println(App)
		fmt.Println(version.Tag)
		fmt.Println(version.Commit)
		fmt.Println(version.Date)
		fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
	},
}

func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&_v,
		"version",
		"v",
		false,
		"show version",
	)
	flags.StringVarP(&_platform,
		"platform",
		"p",
		"windows",
		"now only support 'windows'",
	)
	flags.BoolVar(&_nocopy,
		"nocopy",
		false,
		"only print dependency",
	)
	flags.StringSliceVarP(&_bin,
		"bin",
		"b",
		nil,
		"bin paths, if nil auto find",
	)
	flags.BoolVarP(&_overlay,
		"overlay",
		"o",
		false,
		"when true , if exist overlay dependency file",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
