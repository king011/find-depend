package cmd

import (
	"bytes"
	"debug/pe"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// EmptyDescriptor .
var EmptyDescriptor [20]byte

type _Context struct {
	// 檔案 平臺
	Platform string
	// 是否 只打印 不 copy
	Nocopy bool
	// 依賴的 動態庫 查找 位置
	Bin []string

	Filename string

	// copy 時 是否 直接 覆蓋 已存在 檔案
	Overlay bool
}
type _Node struct {
	// 動態庫 名稱
	Name string
	// 動態庫 路徑
	Path string
}

func newContext(platform string, nocopy, overlay bool, bin []string, filename string) (c *_Context) {
	if !filepath.IsAbs(filename) {
		str, e := filepath.Abs(filename)
		if e != nil {
			log.Fatalln(e)
		}
		filename = str
	}
	fmt.Println("find depend -> ", filename)

	c = &_Context{
		Platform: platform,
		Nocopy:   nocopy,
		Overlay:  overlay,
		Bin:      bin,
		Filename: filename,
	}
	return
}

func (c *_Context) Done() {
	switch c.Platform {
	case "windows":
	default:
		log.Fatalf("not support platform '%s'\n", c.Platform)
		return
	}
	c.windowsDone()
}
func (c *_Context) windowsAppend(arrs []*_Node, keys map[string]*_Node, name string, is64 bool) ([]*_Node, map[string]*_Node) {
	// 已經存在 直接返回
	if _, ok := keys[name]; ok {
		return arrs, keys
	}
	// 創建節點 並添加
	node := &_Node{
		Name: name,
	}
	keys[name] = node
	arrs = append(arrs, node)

	// 查找 路徑
	for _, bin := range c.Bin {
		var filename string
		bin = strings.TrimSpace(bin)
		if strings.HasSuffix(bin, "\\") || strings.HasSuffix(bin, "/") {
			filename = bin + name
		} else {
			filename = bin + "/" + name
		}
		if !path.IsAbs(filename) {
			str, e := filepath.Abs(filename)
			if e != nil {
				continue
			}
			filename = str
		}

		f, e := pe.Open(filename)
		if e != nil {
			continue
		}
		_, ok := f.OptionalHeader.(*pe.OptionalHeader64)
		f.Close()
		if ok == is64 {
			node.Path = filename
			break
		}
	}

	// 遞歸 依賴
	if node.Path != "" {
		_, dlls := c.windowsImports(node.Path)
		if len(dlls) != 0 {
			for _, name := range dlls {
				arrs, keys = c.windowsAppend(arrs, keys, name, is64)
			}
		}
	}

	return arrs, keys
}
func (c *_Context) windowsDone() {
	is64, dlls := c.windowsImports(c.Filename)
	if len(dlls) == 0 {
		fmt.Println("not import anything")
		return
	}
	if len(c.Bin) == 0 {
		c.windowsFindBin(is64)
	}
	fmt.Println("bin -> ", c.Bin)
	arrs := make([]*_Node, 0, len(dlls))
	keys := make(map[string]*_Node)
	for _, name := range dlls {
		arrs, keys = c.windowsAppend(arrs, keys, name, is64)
	}

	dir := path.Dir(c.Filename)
	for _, node := range arrs {
		if node.Path == "" {
			fmt.Println("missing", node.Name)
			continue
		} else {
			fmt.Println(" found ", node.Name, "->", node.Path)
		}
		// 只 打印
		if c.Nocopy {
			continue
		}

		// copy
		e := c.windowsCopyFile(dir+"/"+node.Name, node.Path, is64)
		if e != nil {
			log.Fatalln(e)
		}
	}
	fmt.Println(dir)
}
func (c *_Context) windowsCopyFile(dst, src string, is64 bool) (e error) {
	if !c.Overlay {
		var f *pe.File
		f, e = pe.Open(dst)
		if e == nil {
			_, ok := f.OptionalHeader.(*pe.OptionalHeader64)
			f.Close()
			if ok == is64 {
				return
			}
		}
	}

	var fd *os.File
	fd, e = os.Create(dst)
	if e != nil {
		return
	}
	defer fd.Close()
	var fs *os.File
	fs, e = os.Open(src)
	if e != nil {
		return
	}
	defer fs.Close()

	_, e = io.Copy(fd, fs)
	return
}
func (c *_Context) windowsFindBin(is64 bool) {
	for sys := 'a'; sys <= 'z'; sys++ {
		dir := string(sys) + ":"
		if is64 {
			dir += "/msys64/mingw64/bin"
			filename := dir + "/gcc.exe"
			f, e := pe.Open(filename)
			if e != nil {
				continue
			}
			_, ok := f.OptionalHeader.(*pe.OptionalHeader64)
			f.Close()
			if ok {
				c.Bin = make([]string, 1)
				c.Bin[0] = dir
				return
			}
		} else {
			dir += "/msys64/mingw32/bin"
			filename := dir + "/gcc.exe"
			f, e := pe.Open(filename)
			if e != nil {
				continue
			}
			_, ok := f.OptionalHeader.(*pe.OptionalHeader32)
			f.Close()
			if ok {
				c.Bin = make([]string, 1)
				c.Bin[0] = dir
				return
			}
		}
	}
}

// 返回 pe 導入dll 名單
func (c *_Context) windowsImports(filename string) (is64 bool, dlls []string) {
	// 打開 pe
	f, e := pe.Open(filename)
	if e != nil {
		log.Fatalln(e)
		return
	}
	defer f.Close()

	// 查找 輸入表
	var address uint32
	if pe64, ok := f.OptionalHeader.(*pe.OptionalHeader64); ok {
		is64 = true
		if pe64.DataDirectory[1].Size == 0 {
			return
		}
		address = pe64.DataDirectory[1].VirtualAddress
	} else {
		pe32 := f.OptionalHeader.(*pe.OptionalHeader32)
		if pe32.DataDirectory[1].Size == 0 {
			return
		}
		address = pe32.DataDirectory[1].VirtualAddress
	}

	b := make([]byte, 20)
	var j int64
	var dllname string
	// 查找 輸入表 節
	for i := 0; i < len(f.Sections); i++ {
		section := f.Sections[i]
		if address != section.VirtualAddress {
			continue
		}

		for ; ; j++ {
			_, e = section.ReadAt(b, j*20)
			if e != nil {
				log.Fatalln(e)
				return
			}
			if bytes.Equal(b, EmptyDescriptor[:]) {
				break
			}

			rva := binary.LittleEndian.Uint32(b[4*3:])
			dllname, e = c.windowsReadString(f.Sections, rva)
			if e != nil {
				log.Fatalln(e)
				return
			}
			if dlls == nil {
				dlls = make([]string, 1, 20)
				dlls[0] = dllname
			} else {
				dlls = append(dlls, dllname)
			}
		}
	}
	return
}

// windowsReadString rva位置 讀出一個 \0 結尾的 字符串
func (c *_Context) windowsReadString(sections []*pe.Section, rva uint32) (name string, e error) {
	for i := 0; i < len(sections); i++ {
		section := sections[i]
		if rva >= section.VirtualAddress && rva < section.VirtualAddress+section.Size {
			v := int64(rva - section.VirtualAddress) //+ section.Offset

			b := make([]byte, 0, 100)
			c := make([]byte, 1)
			for {
				_, e = section.ReadAt(c, v)
				if e != nil {
					return
				}
				v++
				if c[0] == 0 {
					break
				}
				b = append(b, c[0])
			}
			name = string(b)
			return
		}
	}
	e = fmt.Errorf("rva [%v] not find section", rva)
	return
}
